﻿namespace Simulación_de_Tanque
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelTanque = new System.Windows.Forms.Panel();
            this.panelLiquido = new System.Windows.Forms.Panel();
            this.btnLlenar = new System.Windows.Forms.Button();
            this.btnVaciar = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panelLEDVaciado = new System.Windows.Forms.Panel();
            this.panelLEDLlenado = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.nudNivel = new System.Windows.Forms.NumericUpDown();
            this.tmrLlenar = new System.Windows.Forms.Timer(this.components);
            this.tmrVaciar = new System.Windows.Forms.Timer(this.components);
            this.panelInterioTanque = new System.Windows.Forms.Panel();
            this.panelGranos = new System.Windows.Forms.Panel();
            this.pbxTanque = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panelTanque.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNivel)).BeginInit();
            this.panelInterioTanque.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxTanque)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.panel1.Controls.Add(this.panelInterioTanque);
            this.panel1.Controls.Add(this.panelTanque);
            this.panel1.Controls.Add(this.pbxTanque);
            this.panel1.Location = new System.Drawing.Point(164, 12);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(183, 352);
            this.panel1.TabIndex = 0;
            // 
            // panelTanque
            // 
            this.panelTanque.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelTanque.Controls.Add(this.panelLiquido);
            this.panelTanque.Location = new System.Drawing.Point(112, 111);
            this.panelTanque.Name = "panelTanque";
            this.panelTanque.Size = new System.Drawing.Size(10, 130);
            this.panelTanque.TabIndex = 1;
            // 
            // panelLiquido
            // 
            this.panelLiquido.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(142)))), ((int)(((byte)(28)))));
            this.panelLiquido.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelLiquido.Location = new System.Drawing.Point(0, 45);
            this.panelLiquido.Name = "panelLiquido";
            this.panelLiquido.Size = new System.Drawing.Size(10, 85);
            this.panelLiquido.TabIndex = 0;
            // 
            // btnLlenar
            // 
            this.btnLlenar.Location = new System.Drawing.Point(13, 16);
            this.btnLlenar.Name = "btnLlenar";
            this.btnLlenar.Size = new System.Drawing.Size(90, 23);
            this.btnLlenar.TabIndex = 1;
            this.btnLlenar.Text = "Llenar";
            this.btnLlenar.UseVisualStyleBackColor = true;
            this.btnLlenar.Click += new System.EventHandler(this.btnLlenar_Click);
            // 
            // btnVaciar
            // 
            this.btnVaciar.Location = new System.Drawing.Point(13, 45);
            this.btnVaciar.Name = "btnVaciar";
            this.btnVaciar.Size = new System.Drawing.Size(90, 23);
            this.btnVaciar.TabIndex = 2;
            this.btnVaciar.Text = "Vaciar";
            this.btnVaciar.UseVisualStyleBackColor = true;
            this.btnVaciar.Click += new System.EventHandler(this.btnVaciar_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(13, 74);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(90, 23);
            this.btnStop.TabIndex = 2;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Gray;
            this.panel4.Controls.Add(this.panelLEDVaciado);
            this.panel4.Controls.Add(this.panelLEDLlenado);
            this.panel4.Controls.Add(this.btnLlenar);
            this.panel4.Controls.Add(this.btnStop);
            this.panel4.Controls.Add(this.btnVaciar);
            this.panel4.Location = new System.Drawing.Point(14, 123);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(144, 112);
            this.panel4.TabIndex = 3;
            // 
            // panelLEDVaciado
            // 
            this.panelLEDVaciado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(32)))), ((int)(((byte)(0)))));
            this.panelLEDVaciado.Location = new System.Drawing.Point(109, 45);
            this.panelLEDVaciado.Name = "panelLEDVaciado";
            this.panelLEDVaciado.Size = new System.Drawing.Size(28, 20);
            this.panelLEDVaciado.TabIndex = 3;
            // 
            // panelLEDLlenado
            // 
            this.panelLEDLlenado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(32)))), ((int)(((byte)(0)))));
            this.panelLEDLlenado.Location = new System.Drawing.Point(109, 19);
            this.panelLEDLlenado.Name = "panelLEDLlenado";
            this.panelLEDLlenado.Size = new System.Drawing.Size(28, 20);
            this.panelLEDLlenado.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 254);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Nivel:";
            // 
            // nudNivel
            // 
            this.nudNivel.Enabled = false;
            this.nudNivel.Location = new System.Drawing.Point(65, 252);
            this.nudNivel.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.nudNivel.Name = "nudNivel";
            this.nudNivel.Size = new System.Drawing.Size(86, 20);
            this.nudNivel.TabIndex = 5;
            this.nudNivel.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nudNivel.ValueChanged += new System.EventHandler(this.nudNivel_ValueChanged);
            // 
            // tmrLlenar
            // 
            this.tmrLlenar.Interval = 20;
            this.tmrLlenar.Tick += new System.EventHandler(this.tmrLlenar_Tick);
            // 
            // tmrVaciar
            // 
            this.tmrVaciar.Interval = 20;
            this.tmrVaciar.Tick += new System.EventHandler(this.tmrVaciar_Tick);
            // 
            // panelInterioTanque
            // 
            this.panelInterioTanque.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.panelInterioTanque.Controls.Add(this.panelGranos);
            this.panelInterioTanque.Location = new System.Drawing.Point(66, 111);
            this.panelInterioTanque.Name = "panelInterioTanque";
            this.panelInterioTanque.Size = new System.Drawing.Size(28, 130);
            this.panelInterioTanque.TabIndex = 2;
            // 
            // panelGranos
            // 
            this.panelGranos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(94)))), ((int)(((byte)(21)))));
            this.panelGranos.BackgroundImage = global::Simulación_de_Tanque.Properties.Resources.granos;
            this.panelGranos.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelGranos.Location = new System.Drawing.Point(0, 45);
            this.panelGranos.Name = "panelGranos";
            this.panelGranos.Size = new System.Drawing.Size(28, 85);
            this.panelGranos.TabIndex = 0;
            // 
            // pbxTanque
            // 
            this.pbxTanque.BackColor = System.Drawing.Color.Transparent;
            this.pbxTanque.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbxTanque.Image = global::Simulación_de_Tanque.Properties.Resources.Tanque_360h;
            this.pbxTanque.Location = new System.Drawing.Point(10, 10);
            this.pbxTanque.Name = "pbxTanque";
            this.pbxTanque.Size = new System.Drawing.Size(163, 332);
            this.pbxTanque.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxTanque.TabIndex = 0;
            this.pbxTanque.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 381);
            this.Controls.Add(this.nudNivel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Simulación de Tanque de Cervecería";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panelTanque.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudNivel)).EndInit();
            this.panelInterioTanque.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxTanque)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pbxTanque;
        private System.Windows.Forms.Button btnLlenar;
        private System.Windows.Forms.Button btnVaciar;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Panel panelTanque;
        private System.Windows.Forms.Panel panelLiquido;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panelLEDVaciado;
        private System.Windows.Forms.Panel panelLEDLlenado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudNivel;
        private System.Windows.Forms.Timer tmrLlenar;
        private System.Windows.Forms.Timer tmrVaciar;
        private System.Windows.Forms.Panel panelInterioTanque;
        private System.Windows.Forms.Panel panelGranos;
    }
}

