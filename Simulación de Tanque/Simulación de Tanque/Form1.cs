﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulación_de_Tanque
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void nudNivel_ValueChanged(object sender, EventArgs e)
        {
            /*
             * RELACIÓN ENTRE LA VARIABLE DEL NIVEL DE LÍQUIDO Y 
             * LA ALTURA DEL PANEL DEL CONTENIDO
             *   nudNivel.Maximum   <====>   panelTanque.Height
             *   nudNivel.Value     <====>   panelLiquido.Height (X)
             */

            panelLiquido.Height = Convert.ToInt32(nudNivel.Value) * panelTanque.Height / Convert.ToInt32(nudNivel.Maximum);

            panelGranos.Height = (int)nudNivel.Value * panelInterioTanque.Height / (int)nudNivel.Maximum;
        }

        private void tmrLlenar_Tick(object sender, EventArgs e)
        {
            if (nudNivel.Value < nudNivel.Maximum)
            {
                // Se puede variar la velocidad cambiando el valor de incremento
                // en el código. Pero el problema es que toca hacer comparaciones
                // más complicadas para evitar que el valor intente incrementar
                // a un número mayor al máximo

                //nudNivel.Value = nudNivel.Value + 2;
                //nudNivel.Value += 5; // Método de incremento con operador +=
                nudNivel.Value++;
            }
            else
            {
                tmrLlenar.Enabled = false;
                panelLEDLlenado.BackColor = Color.FromArgb(0, 32, 0);
            }
        }

        private void tmrVaciar_Tick(object sender, EventArgs e)
        {
            if (nudNivel.Value > nudNivel.Minimum)
            {
                nudNivel.Value--;
            }
            else
            {
                tmrVaciar.Stop();
                panelLEDVaciado.BackColor = Color.FromArgb(0, 32, 0);
            }
        }

        private void btnLlenar_Click(object sender, EventArgs e)
        {
            tmrVaciar.Enabled = false;
            tmrLlenar.Enabled = true;
            panelLEDLlenado.BackColor = Color.LimeGreen;
            panelLEDVaciado.BackColor = Color.FromArgb(0, 32, 0);
        }

        private void btnVaciar_Click(object sender, EventArgs e)
        {
            tmrLlenar.Stop();
            tmrVaciar.Start();
            panelLEDVaciado.BackColor = Color.LimeGreen;
            panelLEDLlenado.BackColor = Color.FromArgb(0, 32, 0);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            panelLiquido.Height = Convert.ToInt32(nudNivel.Value) * panelTanque.Height / Convert.ToInt32(nudNivel.Maximum);
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            tmrLlenar.Stop();
            tmrVaciar.Stop();
            panelLEDLlenado.BackColor = Color.FromArgb(0, 32, 0);
            panelLEDVaciado.BackColor = Color.FromArgb(0, 32, 0);
        }
    }
}
